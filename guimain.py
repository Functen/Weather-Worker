#!/usr/bin/python

import sys
import requests
import json
import tkinter
import tkinter.messagebox
from tkinter import *
global temperatureunit
global countrycode
keycache = open("mykey.txt", "r")
userkey = keycache.read()
keycache.close()
temperatureunit = 'f'
countrycode = ''
initial = tkinter.Tk()
initial.title('Weather-Worker - functendev@protonmail.com')
initial.geometry("800x30+800+820")
def countryset():
    global countrycode
    countryvar = str(select.get())
    if int(countryvar) == 1:
        countrycode = ',us'
    elif int(countryvar) == 2:
        countrycode = ',uk'
    else:
        countrycode = ''

def tempset():
    global temperatureunit
    tempvar = str(selectT.get())
    if int(tempvar) == 1:
        temperatureunit = 'c'
    elif int(tempvar) == 2:
        temperatureunit = 'k'
    else:
        temperatureunit = 'f'

label = Label(initial, text = "search query:")
label.pack(side = LEFT)
querybox = Entry(initial, bd = 5)
querybox.pack(side = RIGHT)

select = IntVar()
regionradio0 = Radiobutton(initial, text='unspecified', variable=select, value=0, command=countryset)
regionradio1 = Radiobutton(initial, text="U.S.", variable=select, value=1, command=countryset)
regionradio2 = Radiobutton(initial, text="U.K.", variable=select, value=2, command=countryset)
regionradio0.pack(side = RIGHT, anchor = E)
regionradio1.pack(side = RIGHT, anchor = E)
regionradio2.pack(side = RIGHT, anchor = E)

selectT = IntVar()
tempradio0 = Radiobutton(initial, text='Fahrenheit', variable=selectT, value=0, command=tempset)
tempradio1 = Radiobutton(initial, text='Celsius', variable=selectT, value=1, command=tempset)
tempradio2 = Radiobutton(initial, text='Kelvin', variable=selectT, value=2, command=tempset)
tempradio0.pack(side = LEFT, anchor = E)
tempradio1.pack(side = LEFT, anchor = E)
tempradio2.pack(side = LEFT, anchor = E)

def weatherget():
    global countrycode
    global temperatureunit
    searchquery = querybox.get()

    try:
        int(searchquery) + 1
    except:
        searchpage = requests.get('http://api.openweathermap.org/data/2.5/weather?q='+searchquery+countrycode+'&APPID='+userkey)
    else:
        searchpage = requests.get('http://api.openweathermap.org/data/2.5/weather?zip='+searchquery+countrycode+'&APPID='+userkey) #different url for zip codes
    requeststatus = searchpage.status_code
    if int(requeststatus) !=200:
        if int(requeststatus) == 401:
            tkinter.messagebox.showinfo("error", 'The API authentication key is missing or was rejected. Did you put an authentication key in mykey.txt? See the readme for details.')
        elif int(requeststatus) == 404:
            tkinter.messagebox.showinfo("error", 'Location not found. Check queries. It is reccommended that you search in area code over name.')
        else:
            tkinter.messagebox.showinfo("error", 'failed to authenticate request or bad query.')
    data = searchpage.text
    parseddata = json.loads(data) #loading json for scraping

    cityname = parseddata["name"]
    cityregion = parseddata["sys"]["country"]

    temp = parseddata["main"]["temp"]
    if temperatureunit in 'f':
        modifiedtemp = (int(temp)-273.15)*(9/5)+32
        tempname = 'Fahrenheit'
        degreesign = 'degrees '
    elif temperatureunit in 'c':
        modifiedtemp = int(temp)-273.15
        tempname = 'Celsius'
        degreesign = 'degrees '
    elif temperatureunit in 'k':
        modifiedtemp = int(temp)
        tempname = 'Kelvin'
        degreesign = ''

    desc = parseddata["weather"][0]["description"]
    windspeed = parseddata["wind"]["speed"]
    try:
        winddegrees = parseddata["wind"]["deg"]
    except:
        winddirection = ''
    else:
        winddegrees = parseddata["wind"]["deg"]
        if int(winddegrees) > 19 and int(winddegrees) < 69.9:
            winddirection='northwest'
        elif int(winddegrees) > 69 and int(winddegrees) < 109.9:
            winddirection='west'
        elif int(winddegrees) > 109 and int(winddegrees) < 159.9:
            winddirection='southwest'
        elif int(winddegrees) > 159 and int(winddegrees) < 199.9:
            winddirection='south'
        elif int(winddegrees) > 199 and int(winddegrees) < 249.9:
            winddirection='southeast'
        elif int(winddegrees) > 249 and int(winddegrees) < 289.9:
            winddirection='east'
        elif int(winddegrees) > 289 and int(winddegrees) < 339.9:
            winddirection='northeast'
        else:
            winddirection='north'

    humidity = parseddata["main"]["humidity"]
 
    lines = [str(int(modifiedtemp))+' '+degreesign+tempname, str(humidity)+'%'+' humidity', desc+', '+str(int(windspeed))+' meters per second winds '+winddirection]
    tkinter.messagebox.showinfo("results for"+' '+cityname+', '+cityregion, '\n'.join(lines))

gobutton = tkinter.Button(initial, text = "go", command = weatherget)
gobutton.pack(anchor = E)

initial.mainloop()
