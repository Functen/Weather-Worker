# Weather-Worker
# Note: Ported to https://github.com/iressa/Weather-Worker-Legacy
A basic weather retriever tool with gui support using the openweathermap.org api developed as I was learning Python.

The only prerequisites to use this software are a Python 3 interpereter, the requests plugin, and an API key.

To get the requests package on Linux, simply type in your command line:

pip install requests

If you want to use the gui version of the script, you need the 'tkinter' package. Most installations of python come with this, but if the program does not run, use the following command:

sudo apt-get install python3-tk

Since the script relies on the openweathermap.org API, you have to get an authentication key to use it.

In order to get an authentication key, you can sign up on openweathermap.org.

https://home.openweathermap.org/users/sign_up

Copy the code under the "API keys" section of your profile (https://home.openweathermap.org/api_keys). A free account is all that is needed.

After you get your key, paste the key into mykey.txt. Every time the script is booted it checks mykey.txt for a key.


If you want to contribute, request anything you think would be helpful in any way, especially in terms of documentation.
API documentation is here: https://openweathermap.org/current

Contact me at functendev@protonmail.com

Any ascii art or modified ascii art used was unsigned, from https://www.asciiart.eu
