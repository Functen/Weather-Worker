#!/usr/bin/python

import sys
import requests
import json
global temperatureunit
keycache = open("mykey.txt", "r")
userkey = keycache.read()
keycache.close()
temperatureunit = 'f'
countryallow= ''
print(' ')
print(' ')
print('weather-worker_1.0, functendev@protonmail.com')
print('=========================================================================================')
def weatherask(countrycode):
    global temperatureunit
    print('Enter city name or area code.')
    search=input(">")
    print("Working...")
    weatherget(searchquery = search, cacheask = 1, countrycode = countryallow)
def weatherget(searchquery, cacheask, countrycode): #cacheask controls the output style and whether the user should be asked to keep the search in their favorites.
    try:
        int(searchquery) + 1
    except: #Checking to see if the given input was either an area code (integer) or a city (string, and thus would be a TypeError if you tried to add 1)
        searchpage = requests.get('http://api.openweathermap.org/data/2.5/weather?q='+searchquery+countrycode+'&APPID='+userkey)
    else:
        searchpage = requests.get('http://api.openweathermap.org/data/2.5/weather?zip='+searchquery+countrycode+'&APPID='+userkey) #different url for zip codes
    try:
        requeststatus = searchpage.status_code
    except:
        print('Connection to API failed. Check internet connection.')
    else:
        if int(requeststatus) != 200: #checking to see if the request was accepted before proceeding
            if cacheask == 0:
                print('Could not get information for favorite location.')
                menu(returnvalue = 1)
            else:
                if int(requeststatus) == 401:
                    print('error: the API authentication key is missing or was rejected. Did you put an authentication key in mykey.txt? See the readme for details.')
                    menu(returnvalue=1)
                elif int(requeststatus) == 404:
                    print('error: location not found. Check spelling and region setting. It is reccommended that you search in area code over city name.')
                    menu(returnvalue=1)
                else:
                    print("error: failed to authenticate API request or bad query")
                    menu(returnvalue=1)
    data = searchpage.text
    parseddata = json.loads(data)

    cityname = parseddata["name"]
    cityregion = parseddata["sys"]["country"]

    temp = parseddata["main"]["temp"]
    if temperatureunit in 'f':
        modifiedtemp = (int(temp)-273.15)*(9/5)+32
        tempname = 'Fahrenheit'
        degreesign = 'degrees '
    elif temperatureunit in 'c':
        modifiedtemp = int(temp)-273.15
        tempname = 'Celsius'
        degreesign = 'degrees '
    elif temperatureunit in 'k':
        modifiedtemp = int(temp)
        tempname = 'Kelvin'
        degreesign = ''

    desc = parseddata["weather"][0]["description"]
    windspeed = parseddata["wind"]["speed"]
    try:
        winddegrees = parseddata["wind"]["deg"]
    except:
        winddirection = ''
    else:
        winddegrees = parseddata["wind"]["deg"]
        if int(winddegrees) > 19 and int(winddegrees) < 69.9:
            winddirection='northwest'
        elif int(winddegrees) > 69 and int(winddegrees) < 109.9:
            winddirection='west'
        elif int(winddegrees) > 109 and int(winddegrees) < 159.9:
            winddirection='southwest'
        elif int(winddegrees) > 159 and int(winddegrees) < 199.9:
            winddirection='south'
        elif int(winddegrees) > 199 and int(winddegrees) < 249.9:
            winddirection='southeast'
        elif int(winddegrees) > 249 and int(winddegrees) < 289.9:
            winddirection='east'
        elif int(winddegrees) > 289 and int(winddegrees) < 339.9:
            winddirection='northeast'
        else:
            winddirection='north'

    humidity = parseddata["main"]["humidity"]

    if 'clear' in desc:
        asci = 1
        ascimage = r"""    |
  \ | /
   \*/
--**O**-- 
   /*\
  / | \
    |"""
    elif 'storm' in desc:
        asci = 1
        ascimage = r"""          .-~~~-.
  .- ~ ~-(       )_ _
 /            /  _/    ~ -.
|           _/  /           \
 \         / __/           .'
   ~- . __/ /_________ . -~
    /  / /__/     / / / /       
        //'
       /"""
    elif 'rain' in desc:
        asci = 1
        ascimage = r"""          .-~~~-.
  .- ~ ~-(       )_ 
 /                     ~ -.
|                           \
 \                         .'
   ~- . _____________ . -~
        /  /  /  /  /
         /   /  /  / """
    elif 'snow' in desc:
        asci = 1
        ascimage = r"""          .-~~~-.
  .- ~ ~-(       )_ 
 /                     ~ -.
|                           \
 \                         .'
   ~- . _____________ . -~
        *  *  *  *  *
         *   *  *  * """
    elif 'cloud' in desc:
        asci = 1
        ascimage = r"""          .-~~~-.
  .- ~ ~-(       )_ 
 /                     ~ -.
|                           \
 \                         .'
   ~- . _____________ . -~"""

    else:
        asci = 0
    if cacheask == 1:
        print('==========')
        print('Results for:', cityname+",", cityregion+':')
        print('Temperature:', int(modifiedtemp), degreesign+tempname)
        print(str(humidity)+'%', 'humidity')
        print(desc+',', windspeed, 'meters per second winds', winddirection)
        if asci == 1:
            print(ascimage)
        print('==========')
        print('Save as favorite? (Y/N)')
        favoritechoice = input('>')
        if favoritechoice in ('y'):
            with open('cache.txt', 'w') as cache:
                try:
                    int(searchquery)
                except:
                    cache.write('{}\n{}\n'.format(str(searchquery),countrycode))
                    cache.close()
                    menu(returnvalue=0)
                else:
                    cache.write('{}\n{}\n'.format(int(searchquery),countrycode))
                    cache.close()
                    menu(returnvalue=0)
        else:
            menu(returnvalue=1)
    else:
        print('---------') #display meant to be run alongside menu, hence no menu()
        print('Current weather for favorite location', cityname+",", cityregion+':')
        print('Temperature:', int(modifiedtemp), degreesign+tempname)
        print(str(humidity)+'%', 'humidity')
        print(desc+',', windspeed, 'meters per second winds', winddirection)
        if asci == 1:
            print(ascimage)
        print('-------------------')
def menu(returnvalue):
    global countryallow
    global temperatureunit
    if returnvalue != 1:
        try:
            cache = open("cache.txt", "r")
            cache_lines = cache.readlines()
            favoriteread = ''.join(line.strip() for line in cache_lines[0])
            countryread = ''.join(line.strip() for line in cache_lines[1]) #getting rid of \n
            cache.close()
        except:
            print('')
        else:
            try:
                weatherget(searchquery = favoriteread, cacheask = 0, countrycode = countryread)
            except:
                print('failed to retrieve weather data for favorite location.')
            else:
                pass


    print('press enter for a city or area search, type "x" to quit, type "e" to specify a region, "q" to clear favorites cache or type "f", "c", or "k" to change temperature units.')
    menupick = input('>')
    if menupick in (' '):
        weatherask(countrycode = countryallow) #keeping the setting and the actual choice for region separate so that countrycode is not a global variable and so that weatherget() can be run by more things than weatherask(), e.g. fetching a favorite.
    elif menupick in ('f'):
        temperatureunit = 'f'
        tempcache = open('tempcache.txt', 'w')
        tempcache.write('f')
        tempcache.close()
        print('The temperature is now in Fahrenheit.')
        menu(returnvalue=1)
    elif menupick in ('c'):
        temperatureunit = 'c'
        tempcache = open('tempcache.txt', 'w')
        tempcache.write('c')
        tempcache.close()
        print('The temperature is now in Celsius.')
        menu(returnvalue=1)
    elif menupick in ('k'):
        temperatureunit = 'k'
        tempcache = open('tempcache.txt', 'w')
        tempcache.write('k')
        tempcache.close()
        print('The temperature is now in Kelvin.')
        menu(returnvalue=1)
    elif menupick in ('x'):
        exit()
    elif menupick in ('q'):
        cache = open('cache.txt', 'w')
        cache.write('{}\n'.format(''))
        cache.close()
        print('The cache has been cleared.')
        menu(returnvalue=1)
    elif menupick in ('e'):
        print('---------------')
        print('You can specify your search query by providing a country. Press enter to cancel. To allow the API to search all countries for cities, type "z" to reset countries.')
        print('United States is the default for area codes if no country is provided.')
        print('----')
        print('United States (us)')
        print('United Kingdom (uk)')
        countrypick = input(">")
        if countrypick in (' '):
            menu(returnvalue=1)
        elif countrypick in ('z'):
            countryallow = ''
            print('You have reset countries.')
            menu(returnvalue=1)
        elif countrypick in ('us', 'united states'):
            countryallow = ',us'
            print('Your country is set to the United States.')
            menu(returnvalue=1)
        elif countrypick in ('uk', 'united kingdom'):
            countryallow = ',uk'
            print('Your country is set to the United Kingdom.')
            menu(returnvalue=1)
        else:
            menu(returnvalue=1)
    else:
        menu(returnvalue=1)

try:
    tempcache = open('tempcache.txt', 'r')
    temperatureunit = tempcache.read()
    tempcache.close()
except:
    pass
else:
    pass
menu(returnvalue=0)
